Installation
============

Install packages
----------------

Debian
^^^^^^

.. code-block:: bash

   apt-get install fusiondirectory-plugin-dyngroup

RHEL
^^^^

.. code-block:: bash

   yum install fusiondirectory-plugin-dyngroup
