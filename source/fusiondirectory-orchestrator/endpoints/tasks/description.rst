Description
===========

Tasks is an endpoints of Orchestrator.

.. note::
  It is reachable via **https://your_fqdn/api/tasks/**

Arguments
---------

- Mail 

You can :
 - Retrieve all tasks of type mail.
 - Process all tasks of type mail and forward the related e-mails.
